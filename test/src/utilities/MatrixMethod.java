package utilities;

public class MatrixMethod {

	public int[][] duplicate(int[][] square) {
		int[][] squareV2 = new int[square.length * 2][square[1].length * 2];
		for (int i = 0; i < square.length; i++) {
			for (int k = 0; k < square[i].length; k++) {
				squareV2[i][k] = square[i][k];
			}
		}
		for (int i = 0; i < square.length; i++) {
			for (int k = 0; i < square[i].length; k++) {
				squareV2[i + square.length][k + square[i].length] = squareV2[i][k];
			}
		}
		return squareV2;
	}
}
