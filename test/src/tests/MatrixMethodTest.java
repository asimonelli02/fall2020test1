package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTest {

	@Test
	void testDuplicate() {
		MatrixMethod testMethod = new MatrixMethod();
		int[][] testArray = { { 1, 2, 3 }, { 4, 5, 6 } };
		int[][] testArrayV2 = { { 1, 2, 3, 1, 2, 3 }, { 4, 5, 6, 4, 5, 6 } };
		assertArrayEquals(testArrayV2, testMethod.duplicate(testArray));
	}

}
