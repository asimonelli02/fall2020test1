package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {

	@Test
	void testGet() {
		Car testGetter = new Car(4);
		assertEquals(4, testGetter.getSpeed());
		assertEquals(0, testGetter.getLocation());
	}

	@Test
	void testMoveRight() {
		Car testRight = new Car(4);
		assertEquals(testRight.getLocation() + testRight.getSpeed(), testRight.getLocation());
	}

	@Test
	void testMoveLeft() {
		Car testLeft = new Car(4);
		assertEquals(testLeft.getLocation() - testLeft.getSpeed(), testLeft.getLocation());
	}

	@Test
	void testAccelerate() {
		Car testAccelerate = new Car(4);
		assertEquals(testAccelerate.getSpeed() + 1, testAccelerate.getSpeed());
	}

	@Test
	void testDecelerate() {
		Car testDecelerate = new Car(4);
		assertEquals(testDecelerate.getSpeed() - 1, testDecelerate.getSpeed());
	}

}
